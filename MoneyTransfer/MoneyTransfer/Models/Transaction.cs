﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MoneyTransfer.Models
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }
        public string Sender { get; set; }
        public string Receiver { get; set; }
        public int TransferSum { get; set; }
        public DateTime ActionDate { get; set; }
    }
}