﻿using System.ComponentModel.DataAnnotations;

namespace MoneyTransfer.ViewModels
{
    public class DepositViewModel
    {
        [Required]
        [Display(Name = "AccountId")]
        public string INN { get; set; }
        
        [Required]
        [Display(Name = "Sum")]
        public int Money { get; set; }
    }
}