﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class TransferController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly ApplicationContext _context;

        public TransferController(UserManager<User> userManager, ApplicationContext context)
        {
            _userManager = userManager;
            _context = context;
        }
        
        [HttpGet]
        public IActionResult Deposit()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Deposit(DepositViewModel model)
        {
            User user1 = await _userManager.FindByNameAsync(User.Identity.Name);
            
            if (user1.Balance < 100)
            {
                ModelState.AddModelError(string.Empty, "not enough money");
                return View();
                
            }
            else
            {
                
                user1.Balance -= model.Money;
            }
            
            
            User user2 = _context.Users.FirstOrDefault(u => u.AccountId == model.INN);

            if (user2 != null)
            {
                
                user2.Balance += model.Money;
            }
            else
            {
                ModelState.AddModelError(string.Empty, "this account doesn't exist");
                return View();
            }
            

            Transaction transaction = new Transaction()
            {
                Sender = user1.ToString(),
                Receiver = user2.ToString(),
                TransferSum = model.Money,
                ActionDate = DateTime.Now                
            };

            _context.Transactions.Add(transaction);
            _context.SaveChanges();
            
            return View();

        }
        
        public async Task<IActionResult> TransactionList()
        {
            User user = await _userManager.FindByNameAsync(User.Identity.Name);
            List<Transaction> transaction = _context.Transactions.Where(n => n.Receiver == user.ToString() || n.Sender == user.ToString()).ToList();
            return View(transaction);
        }
    }
}